# Blockchain developer

This subject aim to prove your divine power in the blockchain by developing a smart contract that
interacts with different protocols.

## Subject

### Make the smart contract

You have to develop a smart contract that implements the already existing interface `IPulsarArbitrage`.
The function `executeArbitrage` have to :
 - Make a flashloan on AAVE
 - Execute an arbitrary number of swap through uniswap v2 pool with the borrowed amount
 - Reimburse the flashloan
 
**Tips:** Lot of DEX are forked from Uniswap v2 and should be compatible with your code. Moreover it should be usefull to
have two or more pool of the same pair. Also note the fee can differ across forks, we just want you to manage DEX that 
take 0.3% fee like the original uniswap v2 protocol does (SushiSwap use the same value).

### Make the tests

You have to write a typescript test file using hardhat and ethers that:
 - Deploy the contract
 - Wrap some ETH to WETH
 - Send WETH to the contract (this will be usefull for next step)
 - Get some pool from the UniswapV2 factory (ex: WETH/WBTC ; WBTC/USDT ; USDT/WETH)
 - Execute the smart contract (it's not easy to make a profitable arbitrage, that's why it's usefull to send WETH to the contract before calling it, in order to have more funds to reimburse the flashloan)
 - Ensure the transaction success

## Requirements & Goal

 - A minimal hardhat config is already setup
 - You should get an archive node (for exemple in alchemy.io) and setup the env variable `ARCHIVE_NODE` in order to be able to fork the mainnet
 - Your contract should not use hardcoded address to interact with another protocol (pass it to constructor if need)
 - You have to use `yarn` instead of npm  
 - You have to use the npm package `ethers` to make your tests (already installed)
 - You can use any other lib you think relevant
 - You have to create test & deploy scripts in typescript
 - Your main smart contract have to be named `PulsarArbitrage` and implements `IPulsarArbitrage`
 - Feel free to improvise if you need to and comment your code to explain your choices

Good luck.

In order to not influence others candidates, do not fork the project in a public repo. Send us a zip of your solution or add us to a private repo.
